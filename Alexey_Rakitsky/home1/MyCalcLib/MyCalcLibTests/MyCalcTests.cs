﻿using System;
using NUnit.Framework;
using MyCalcLib;

namespace MyCalcLibTests
{
    [TestFixture]
    public class MyCalcTests
    {
        private MyCalc calc;

        [SetUp]
        public void SetUp()
        {
            calc = new MyCalc();
        }

        // this tests should fail, code has a mistake
        [TestCase(2, 3, 5)]
        [TestCase(-8, 7, -1)]
        [TestCase(1, 7, 8)]
        public void SumTest(int x, int y, int expected)
        {
            var actual = calc.Sum(x, y);
            Assert.AreEqual(expected, actual);
        }

        [TestCase(8, 12, -4)]
        [TestCase(-3, 3, -6)]
        [TestCase(18, 0, 18)]
        public void SubTest(int x, int y, int expected)
        {
            var actual = calc.Sub(x, y);
            Assert.AreEqual(expected, actual);
        }

        [TestCase(8, 4, 32)]
        [TestCase(3, -8, -24)]
        [TestCase(0, 3, 0)]
        public void MulTest(int x, int y, int expected)
        {
            var actual = calc.Mul(x, y);
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void DivTest_11divideTo0_throwsArgumentException()
        {
            double x = 11;
            double y = 0;
            Assert.Throws<ArgumentException>(() => calc.Div(x, y));
        }

        [Test]
        public void DivideTest_4divideTo2_returnsValueTypeOfDouble()
        {
            //AAA rule
            //arrange
            double x = 4;
            double y = 2;

            //act
            var result = calc.Div(x, y);

            //assert
            Assert.IsInstanceOf<double>(result);
        }

        [TearDown]
        public void TearDown()
        {
            calc = null;
        }
    }
}
