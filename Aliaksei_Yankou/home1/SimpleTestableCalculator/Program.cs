﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleTestableCalculator
{
	class Program
	{
		static void Main(string[] args)
		{
			var calc = new Calculator();
			Console.WriteLine(calc.Divide(int.MaxValue, 0));
		}
	}
}
