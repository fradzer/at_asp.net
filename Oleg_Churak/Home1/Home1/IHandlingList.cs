﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Home1
{
    public interface IHandlingList
    {
        List<int> GetListOfInts(int count);
    }
}
